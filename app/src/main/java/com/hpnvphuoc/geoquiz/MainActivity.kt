package com.hpnvphuoc.geoquiz

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView
import android.widget.Toast
import androidx.core.view.isVisible

class MainActivity : AppCompatActivity() {
    private lateinit var trueButton:Button
    private lateinit var falseButton:Button
    private lateinit var nextButton:ImageButton
    private lateinit var questionTextView:TextView
    private lateinit var prevButton:ImageButton
    private val questionBank = listOf(
        Question(R.string.question_australia,true),
        Question(R.string.question_oceans,true),
        Question(R.string.question_mideast,false),
        Question(R.string.question_africa,false),
        Question(R.string.question_americas,true),
        Question(R.string.question_asia,true))
    private var currentIndex=0
    private var isAnswer=false
    private var trueAnswer=0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        trueButton=findViewById(R.id.true_button)
        falseButton=findViewById(R.id.false_button)
        nextButton=findViewById(R.id.next_button)
        prevButton=findViewById(R.id.prev_button)
        questionTextView=findViewById(R.id.question_text_view)

        //challenge 1 set next button visible when textview is click
//        nextButton.isVisible=false
//
//        questionTextView.setOnClickListener{
//            nextButton.isVisible=true
//        }
        //end challange
        updateQuestion()

        trueButton.setOnClickListener { view: View ->
            checkAnswer(true);
            disableButton(true)
        }
        falseButton.setOnClickListener { view: View ->
            checkAnswer(false)
           disableButton(true)
        }
        nextButton.setOnClickListener { view:View->
            currentIndex=(currentIndex+1)%questionBank.size
           updateQuestion()
           disableButton(false)
        }
        //challenge 2: add prev button
        prevButton.setOnClickListener { view:View->
            currentIndex=if(currentIndex-1<0){
                questionBank.size-1
            }else{
                currentIndex-1
            }
            updateQuestion()
        }
        updateQuestion()
    }
    private fun updateQuestion(){
        val questionTextResId=questionBank[currentIndex].textResId
        questionTextView.setText(questionTextResId)
    }
    private fun checkAnswer(userAnswer:Boolean){
        val correctAnswer=questionBank[currentIndex].answer
        val messageResId= if(userAnswer==correctAnswer){
            trueAnswer=trueAnswer+1
            R.string.correct_toast
        }else{
            R.string.incorrect_toast
        }
        if(currentIndex==questionBank.size-1){
            val percent_message="You have ${(trueAnswer/questionBank.size)*100} percent of correct answer"
            Toast.makeText(this,percent_message,Toast.LENGTH_LONG).show()
        }else{
            Toast.makeText(this,messageResId,Toast.LENGTH_SHORT).show()
        }
    }
    private fun disableButton(isDisable:Boolean=false){
        trueButton.isEnabled=!isDisable
        falseButton.isEnabled=!isDisable
    }

}